package driver;


public class Emulator {

    public static final String platformName = "Android";
    public static final String deviceName = "Android Emulator";
    public static final String avd = "Nexus5";
    public static final String platformVersion = "9.0";
    public static final String URL = "http://127.0.0.1:4723/wd/hub";


    public static final String appPackage = "com.flipkart.android";
    public static final String appActivity = "com.flipkart.android.activity.LoginActivity";
}
