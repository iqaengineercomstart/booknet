package steps;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class StartRegistration {
//WebDriver driver;


  public static void main(String args[]) throws MalformedURLException {
    //начало любого теста
    DesiredCapabilities config = new DesiredCapabilities();
    config.setCapability("platformName", "android");
    config.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
    config.setCapability("appPackage", "ua.com.rozetka.shop");
    config.setCapability("appActivity", ".screen.MainActivity");

    AndroidDriver<AndroidElement> at = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), config);
    at.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    //driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), config);

//@Test

    MobileElement buttonMore = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/graph_more");
    // buttonRegistration.isDisplayed();
    MobileElement textButtonMore = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/smallLabel");
    //MobileElement buttonRegistration = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/item_menu_auth_tv_sign_up");
    //MobileElement textButtonRegistration = (MobileElement) at.findElementByLinkText("Ще");
    if (textButtonMore.isDisplayed()) {
      System.out.println("text is correct");
    } else {
      System.out.println("text is wrong");
    }

    if (buttonMore.isDisplayed()) {
      System.out.println("graph_more button is displayed successfully");
      buttonMore.click();
    } else {
      System.out.println("graph_more button is missed");
    }
    ;
    MobileElement buttonRegistration = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/item_menu_auth_tv_sign_up");
    if (buttonRegistration.isDisplayed()) {
      System.out.println("registration button is displayed successfully");
      buttonRegistration.click();
    } else {
      System.out.println("registration button is incorrect");
    }
    //registration tab is displayed
    MobileElement tabRegistration = (MobileElement) at.findElementByAccessibilityId("Реєстрація");
    boolean isDisplayed = tabRegistration.isDisplayed();
    //fill form

    MobileElement lastName = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/sign_up_et_last_name");
    lastName.sendKeys("Петренко");
    String lastNameContent = lastName.getAttribute("text");

    MobileElement firstName = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/sign_up_et_first_name");
    firstName.sendKeys("Иван");
    String firstNameContent = firstName.getAttribute("text");

    MobileElement phone = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/sign_up_et_phone");
    phone.sendKeys("956299202");
    String phoneNumber = phone.getAttribute("text");

    MobileElement email = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/sign_up_et_email");
    email.sendKeys("k4872319+4@mail.com");
    String emailAddress = email.getAttribute("text");
    System.out.println("email is+" + email.getText());

    MobileElement password = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/sign_up_et_password");
    password.sendKeys("1212Qq");
    String passwordValid = password.getAttribute("text");

    MobileElement buttonSignup = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/sign_up_b_register");
    if (buttonSignup.isDisplayed()) {
      System.out.println("signup button is displayed successfully");
      buttonSignup.click();
    } else {
      System.out.println("signup button is enabled");
    }
    at.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    MobileElement fieldCode = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/need_code_et_code");
        if (fieldCode.isDisplayed()) {
      System.out.println("fieldCode is displayed successfully");
      fieldCode.sendKeys("1234");
          }
    else {
      System.out.println("fieldCode is enabled");
    }
    MobileElement confirm = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/need_code_tv_confirm");
    confirm.click();

    MobileElement invalidCode = (MobileElement) at.findElementById("android:id/message");
    boolean isDispayed = invalidCode.isDisplayed();
  }
//  WebElement buttonRegistration = el.findElement(By.id ("ua.com.rozetka.shop:id/graph_more"));

  }

