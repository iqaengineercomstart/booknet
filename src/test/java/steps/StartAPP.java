package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class StartAPP {

    /*@When("^Open app$")
    public void Open_app() throws MalformedURLException {
        DesiredCapabilities config = new DesiredCapabilities();
        config.setCapability("platformName", "android");
        config.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        config.setCapability("appPackage", "ua.com.rozetka.shop");
        config.setCapability("appActivity", ".screen.MainActivity");

        AndroidDriver<AndroidElement> at = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), config);
        at.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), config);
           }*/

   @Then("^App is opened successfully$")
    public static void App_is_opened_successfully() throws MalformedURLException {
        DesiredCapabilities config = new DesiredCapabilities();
        config.setCapability("platformName", "android");
        config.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        config.setCapability("appPackage", "ua.com.rozetka.shop");
        config.setCapability("appActivity", ".screen.MainActivity");
        AndroidDriver<AndroidElement> at = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), config);
        at.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MobileElement search = (MobileElement) at.findElementById("ua.com.rozetka.shop:id/item_main_v_search");
        boolean isDisplayed = search.isDisplayed();
        //boolean isViewed = search.isDisplayed();

    }


}

